Here are a few resources that may be useful:

1. The Classics

Many of the ML courses use the same datasets and have done so for years, 
perhaps because there was a dearth of high quality options at one point.
Here you'll find digit recognition datasets like MNIST and the Iris plant
dataset, many others. Downside here is many of them are for object recognition
problems so you are already jumping to convolutional neural networks, not 
necessarily the best place to get your feet wet.

https://en.wikipedia.org/wiki/List_of_datasets_for_machine-learning_research


2. Kaggle

People add to kaggle when they'd like someone to work on their data, there is 
lots of interesting stuff to investigate. My guess is that quality will be 
much more varied:

https://www.kaggle.com/datasets


3. Credit Default

I did a project that looked at credit approvals and defaults across three countries.
I like this example b/c it doesn't require jumping to object detection which
may involve more complicated matrices, networks, and code. Some of the data is 
on kaggle, some at a university site:

Australia: http://archive.ics.uci.edu/ml/datasets/statlog+(australian+credit+approval
Taiwan: https://www.kaggle.com/uciml/default-of-credit-card-clients-dataset
Germany: https://archive.ics.uci.edu/ml/datasets/statlog+(german+credit+data)

My project code for this was here:

https://gitlab.com/djpetersen/ml_resources/-/blob/master/Credit_Default_Capstone_-_Code_Updated.ipynb

I will look around for a writeup of results.


4. Baseball salaries and winning rates

Did another project looking at contribution of salaries to winning. The data is here:

http://www.seanlahman.com/baseball-archive/statistics/

My project is here:

https://gitlab.com/djpetersen/ml_resources/-/blob/master/baseball_salary_analysis.ipynb


